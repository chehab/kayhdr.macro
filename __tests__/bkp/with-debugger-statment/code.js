const log = require('kayhdr.macro')

let vitae = 'tincidunt'

function vestibulum() {
  vitae = "lacus"

  log.debug("Nullam congue nisl eget nisi blandit ultrices.")

  let hendrerit = 'lorem, ' + vitae
}

log.debug("Proin tristique lorem at velit pharetra porta.")

vestibulum()

log.debug("Integer a nisi at libero tincidunt hendrerit.")

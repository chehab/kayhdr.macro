const x = 2 + 3;

class BazFoo {
  foobarBaz(a, b) {
    console.log("%c[debug]\t[BazFoo:foobarBaz()]\n%cFoobar baz faz", "color: #423A2D;font-size: 9px;", "color: #423A2D;");
    debugger;
    return a * b;
  }

}

const y = new BazFoo(x, 2);
const z = y.foobarBaz(x, y);

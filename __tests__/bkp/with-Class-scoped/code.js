const log = require('kayhdr.macro')

const x = 2 + 3

class BazFoo {
  foobarBaz(a, b) {
    log.debug('Foobar baz faz')
      .inspect(a, b)
      .scope()

    return a * b
  }
}


const y = new BazFoo(x, 2)

const z = y.foobarBaz(x, y)

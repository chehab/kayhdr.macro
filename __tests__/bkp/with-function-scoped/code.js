const log = require('kayhdr.macro')

const x = 2 + 3

function foobarBaz(a, b) {
  log.debug('Foobar baz faz')

  return a * b
}

const y = foobarBaz(x, 2)

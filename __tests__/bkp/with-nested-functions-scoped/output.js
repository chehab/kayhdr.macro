const x = 2 + 3;

function foobarBaz(a, b) {
  const m = 2 % 3;

  function fooPoo(a, b) {
    function bazFaz(a, b) {
      console.log("%c[debug]\t[foobarBaz():fooPoo()]\n%cfooPoo foobarBaz fazfaz", "color: #423A2D;font-size: 9px;", "color: #423A2D;");
      debugger;
      return a * b;
    }
    return bazFaz(a * b, 0);
  }

  return fooPoo(m);
}

const y = foobarBaz(x, 2);

const log = require('kayhdr.macro')

const x = 2 + 3

function foobarBaz(a, b) {
  const m = 2 % 3

  function fooPoo(a, b) {
    function bazFaz(a, b) {
      log.debug('fooPoo foobarBaz fazfaz')

      return a * b
    }

    return bazFaz(a * b, 0)
  }

  return fooPoo(m)
}

const y = foobarBaz(x, 2)

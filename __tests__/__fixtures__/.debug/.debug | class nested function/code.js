const log = require('kayhdr.macro')

/**
 * Donec condimentum justo sit amet vestibulum porta.
 */
class Morbi {

  /**
   * Praesent finibus augue in ante.
   */
  nunc() {
    function mauris() {
      log.debug('Curabitur nec mi ac ligula pretium ultrices.')

      return true
    }

    return mauris()
  }
}

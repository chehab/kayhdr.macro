const log = require('kayhdr.macro')

function nulla(dolor) {
  let amet = dolor.diam('consectetur')

  log.debug('Lorem ipsum dolor sit amet, consectetur adipiscing elit.')

  let elit = dolor.nunc('adipiscing', amet)

  return { elit, amet }
}

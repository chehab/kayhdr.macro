function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }
  return obj;
}

/**
 * Donec condimentum justo sit amet vestibulum porta.
 */
class Morbi {
  constructor() {
    _defineProperty(this, "nunc", () => {
      // Donec efficitur est a porttitor ultricies.
      console.log(
        "%c  debug  %c  [Morbi{}::nunc:(\u019B)]\t\t%c[code.js:13:4]%c\n%cProin scelerisque ex nec aliquam volutpat.\n\n%c<__tests__/__fixtures__/.debug/.debug | class property/code.js:13:4>",
        "background: YELLOW; color: DARKRED; border-radius: 10px;text-transform: uppercase;;",
        "color: #000; background: transparent;",
        "color: gray;float:right;",
        "clear: both;",
        "color: black;font-size: 16px;",
        "font-size: 9px; color: black; background: #fff;"
      );
      debugger;
    });
  }
}

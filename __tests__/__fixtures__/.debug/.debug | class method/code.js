const log = require('kayhdr.macro')

/**
 * Donec condimentum justo sit amet vestibulum porta.
 */
class Morbi {
  /**
   * Praesent finibus augue in ante.
   */
  nunc() {
    // Donec efficitur est a porttitor ultricies.

    log.debug('Proin scelerisque ex nec aliquam volutpat.')

  }
}

const log = require('kayhdr.macro')

const LOREM = 'ipsum'

function nulla() {
  const dolor = new Morbi(LOREM)

  log.debug('Lorem ipsum dolor sit amet, consectetur adipiscing elit.')

  let amet = dolor.diam('consectetur')
  let elit = dolor.nunc('adipiscing', amet)

  return { elit, amet }
}

log.debug('Aliquam at arcu vitae purus tempor elementum.')

nulla()


/**
 * Donec condimentum justo sit amet vestibulum porta.
 */
class Morbi {

  /**
   * Praesent finibus augue in ante.
   */
  nunc() {
    // Donec efficitur est a porttitor ultricies.

    log.debug('Proin scelerisque ex nec aliquam volutpat.')

    function mauris() {
      for (let i = 2; i < n; i++) {
        log.debug('Curabitur nec mi ac ligula pretium ultrices.')

        if (n % i == 0) return false
      }

      return true
    }

    return mauris()
  }

  diam(risus) {
    // Ut semper risus sed orci malesuada congue.
    log.debug('Mauris feugiat metus nec orci lobortis pharetra.')

    return () => {
      log.debug('Nullam at diam sed erat molestie rhoncus.')

      switch (risus) {
        case 18:
          return "Ut id dolor aliquet, posuere arcu in, tincidunt lacus."

        case "18":
          return "Sed vitae dolor at urna porttitor interdum ut sit amet neque."

        default:
          return "Ut varius lorem hendrerit felis mattis ultrices."
      }
    }
  }
}

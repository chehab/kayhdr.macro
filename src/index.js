const { createMacro } = require('babel-plugin-macros')

module.exports = createMacro(require('./macro'))

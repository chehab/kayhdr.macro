const _ = require('lodash')
const path = require('path')
const babelTypes = require('@babel/types')

const { consoleMessageFormatter } = require('./formaters')


module.exports = class LogBuilder {

  /**
   * handle Default Imports
   *
   * @param defaultImport
   * @param options
   */
  static handleDefault(defaultImport, options) {
    const hasDefaultImport = Array.isArray(defaultImport)

    if (hasDefaultImport) {
      defaultImport.forEach((node) => {
        if (babelTypes.isMemberExpression(node.parentPath)) {
          LogBuilder
            .of(node, options)
            .build()

        }
      })
    }
  }

  static of(...args) {
    return new LogBuilder(...args)
  }

  /**
   * get logger level
   *
   * @returns {string}
   */
  get level() {
    return _.get(this.parentPath, 'parentPath.node.property.name', 'log')
  }

  /**
   *
   * @returns {*}
   */
  get parent() {
    return _.get(this.parentPath, 'parentPath.parentPath')
  }

  get args() {
    return _.get(this.parentPath, 'parentPath.container.arguments', [])
  }

  get loc() {
    if (_.has(this.headStatment, 'container.loc.start')) {
      const { line, column } = this.headStatment.container.loc.start
      return `${line}:${column}`
    } else if (_.has(this.headStatment, 'node.loc.start')) {
      const { line, column } = this.headStatment.node.loc.start
      return `${line}:${column}`
    }
  }

  get message() {
    const args =  this.args
    if (args.length) {
      const [msg] = args
      if (babelTypes.isStringLiteral(msg)) {
        return msg.value
      }
    }
  }

  get log() {
    const namespace = this.namespace

    return {
      withIcons: false,

      level: this.level,
      namespace: namespace.repr,
      loc: this.loc || namespace.locStart,
      message: this.message || '',

      scope: null,
      inspect: null,

      file: namespace.file,
    }
  }

  constructor(node, options) {
    this.parentPath = node
    this.options = options
    this._deduceStatement()
  }

  build() {
    this._deduceNamespace()

    switch (this.level) {
      case 'debugger':
        return this.debugger()
      case 'debug':
        return this.debug()
      case 'log':
        return this.debug()
      default:
        return 0
    }
  }

  debugger() {
    const consoleLogExpression = babelTypes.memberExpression(
      babelTypes.identifier("console"),
      babelTypes.identifier("log")
    )

    const nameLog = babelTypes.stringLiteral(this.name + ':')

    const consoleLogArgs = [
      nameLog,
      ...this.args,
    ]

    // (this.parentPath.node.arguments || []).forEach((a) => logArgs.push(a))

    const consoleLog = babelTypes.callExpression(
      consoleLogExpression,
      consoleLogArgs,
    );

    // TOOD
    this.headStatment.replaceWithMultiple([
      babelTypes.expressionStatement(consoleLog),
      babelTypes.debuggerStatement()
    ]);
  }

  debug() {
    const consoleLogExpression = babelTypes.memberExpression(
      babelTypes.identifier("console"),
      babelTypes.identifier("log")
    )

    const { simple: consoleLogArgs } = consoleMessageFormatter(this.log)

    const consoleLog = babelTypes.callExpression(
      consoleLogExpression,
      consoleLogArgs,
    );

    this.headStatment.replaceWithMultiple([
      babelTypes.expressionStatement(consoleLog),
      babelTypes.debuggerStatement()
    ]);
  }


  _deduceStatement() {
    let nextParentPath = this.parentPath
    let isExpressionStatement = false
    let memberExpression = []

    while (!isExpressionStatement) {
      isExpressionStatement = babelTypes.isExpressionStatement(nextParentPath)

      if (isExpressionStatement) {
        break
      }

      if (babelTypes.isMemberExpression(nextParentPath)) {
        memberExpression.push(nextParentPath)
      }

      nextParentPath = nextParentPath.parentPath
    }

    this.headStatment = nextParentPath
    this.memberExpression = memberExpression
  }


  _deduceNamespace() {
    const t = babelTypes
    const locStart = this.loc
    this.stack = this._deduceStack()

    this.namespace = this.stack
      .reduce(stackReducer, { locStart })

    return this.namespace

    function _repr(previuse, repr, postfix='', separtor='') {
      const tmp = _.isEmpty(previuse)
        ? ''
        : `${separtor}${previuse}`

      return `${repr}${postfix}${tmp}`
      }

    function stackReducer(a, n, ndx) {
      if (t.isProgram(n.node)) {
        a.file = n.file

        a.fileRepr = `${n.file.base}:${locStart}`

        a.repr = _.isEmpty(a.repr)
          ? a.fileRepr
          : a.repr
      }

      if (t.isArrowFunctionExpression(n.node)) {
        a.repr = _repr(a.repr, '(ƛ)', '', ':')
      }

      if (t.isFunctionDeclaration(n.node)) {
        a.repr = _repr(a.repr, n.id, '()', ':')
      }

      if (t.isClassMethod(n.node)) {
        a.repr = _repr(a.repr, n.name, '()', ':')
      }

      if (t.isClassPrivateMethod(n.node)) {
        a.repr = _repr(a.repr, `#${n.name}`, '()', ':')
      }

      if (t.isClassDeclaration(n.node)) {
        a.repr = _repr(a.repr, n.id, '{}', '::')
      }

      return a
    }
  }


  _deduceStack() {
    let nextNode = this.parentPath
    let notProgramRoot = true
    let nodeStack = []

    while (notProgramRoot && nextNode) {
      const ns = deduceNodeStack(nextNode)

      nodeStack.push(ns)

      notProgramRoot = !babelTypes.isProgram(nextNode)

      nextNode = nextNode.parentPath
    }

    return nodeStack

    function deduceNodeStack(node) {
      let namespace = {
        node: node,
        nodeType: node.type,
        name: _.get(node, 'scope.block.key.name'),
        id: _.get(node, 'scope.block.id.name'),
        kind: _.get(node, 'scope.block.kind'),
        params: _.get(node, 'scope.block.params'),
        bindings: _.get(node, 'scope.bindings'),
      }

      if (babelTypes.isProgram(node)) {
        const { line, column } = node.parent.loc.start

        const filePath = path.relative(
          _.get(node, 'hub.file.opts.root'),
          _.get(node, 'hub.file.opts.filename')
        )

        namespace.file = path.parse(filePath)
        namespace.file.locStart = `${line}:${column}`
      }

      return namespace
    }
  }


  _getLogOpt() {
    const opt1Type = this.parentPath.context.parentPath.context.parentPath.type
    const opt1Name = this.parentPath.context.parentPath.context.parentPath.container.property.name

    const opt2Type = this.parentPath.context.parentPath.context.parentPath.type
    const opt2Name = this.parentPath.context.parentPath.context.parentPath.container.property.name

    /*

      log.debug("........")
      ---^(1)-^(2)
        .inspect(a, b, c)
      ---^(3)-^(4)
        .scope()
      ---^(5)-^(6)
        .assert(a !== c)
      ---^(7)-^(8)

     (1) => this.parentPath
     (2) => this.parentPath.context.parentPath

     (4) => this.parentPath.context.parentPath.context.parentPath
     (5) => this.parentPath.context.parentPath.context.parentPath.context.parentPath

     (4) => this.parentPath.context.parentPath.context.parentPath.context.parentPath.context.parentPath
     (5) => this.parentPath.context.parentPath.context.parentPath.context.parentPath.context.parentPath.context.parentPath

     (6) => (5) <<= .context.parentPath
     (7) => (6) <<= .context.parentPath

     ({
       type: this.parentPath.context.parentPath.context.parentPath.type,
       name: this.parentPath.context.parentPath.context.parentPath.container.property.name,
       nextType: this.parentPath.context.parentPath.context.parentPath.context.parentPath.type,
       nextName: this.parentPath.context.parentPath.context.parentPath.context.parentPath.context.parentPath.context.parentPath,
     })
    */

  }


  clearNode() {}
}

// [code.js:15:8]
// [code.js:15:8  ./src/foo/bar/]
// [FooBar:bazFoo()  code.js:15:8]
// [FooBar:bazFoo()  ./src/foo/bar/code.js:15:8]
// [FooBar:bazFoo()  ./src/foo/bar/code.js:15:8]

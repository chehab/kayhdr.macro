const babelTypes = require('@babel/types')

const paramsDefault = {
  loc: null,
  args: null,
  that: null,
  file: null,
  level: 'log',
  scope: null,
  message: '',
  inspect: null,
  namespace: 'n/a',
}

module.exports.consoleMessageFormatter = function consoleMessageFormatter(params) {
  const {
    loc,
    args, // [parentPath,...]
    that,
    file,
    level,
    scope, // [parentPath,...]
    namespace,
    message,
    inspect, // [parentPath,...]
  } = {
    ...paramsDefault,
    ...params,
  }

  const reprArgs = (function () {
    return ''
  })()

  const reprInspect = (function () {
    return ''
  })()


  const f_lvl = `%c  ${level}  `
  const f_ns = `%c  [${namespace}]\t\t`
  const f_loc = `%c[${file.base}:${loc}]%c`
  const f_msg = `\n%c${message}\n`
  const f_arg = args ? `\n%c◇ Args:\n\t${reprArgs}` : ''
  const f_slf = that ? `\n%c◇ %s{}this: %o` : ''
  const f_ins = inspect ? `\n%c◇ Inspect:\n\t${reprInspect}` : ''
  const f_scp = scope ? `\n%c◇ Scope:\n\t%o` : ''
  const f_src = (loc && file) ? `\n%c<${file.dir}/${file.base}:${loc}>` : ''

  const logTemplate = `${f_lvl}${f_ns}${f_loc}${f_msg}${f_arg}${f_slf}${f_ins}${f_scp}${f_src}`

  const levelStyle = getLevelStyle(level)
  const messageStyle = getMessageStyle(level)
  const namespaceStyle = 'color: #000; background: transparent;'
  const locStyle = 'color: gray;float:right;'
  const locResetStyle = 'clear: both;'

  // common args
  const commonArgs = []

  const groupTitle = babelTypes.stringLiteral(`[${level}]\t[${namespace}]`)

  const groupedArgs = [
    // log message
    babelTypes.stringLiteral(`${f_lvl}${f_ns}${f_loc}`),
    // $message style
    babelTypes.stringLiteral('color: #423A2D;'),
  ]

    // babelTypes.stringLiteral(`%c [${level}]\t[${namespace}]\t\t\t${logLoc}\n%c${message}${logInspect}${logScope}${srcLoc}`),
  const simpleArgs = [
    // log message
    babelTypes.stringLiteral(logTemplate),
    babelTypes.stringLiteral(levelStyle),
    babelTypes.stringLiteral(namespaceStyle),
    babelTypes.stringLiteral(locStyle),
    babelTypes.stringLiteral(locResetStyle),
    babelTypes.stringLiteral(messageStyle),
  ]

  if (inspect) {
    // $inspect style
    commonArgs.push(babelTypes.stringLiteral('color:#692580;'))
    // // $inspect object
    // args.push(inspect)
  }

  if (scope) {
    // $scope style
    commonArgs.push(babelTypes.stringLiteral('color:#692580;'))
    // // $scope object
    // args.push(scope)
  }

  if (loc) {
    // $loc style
    commonArgs.push(babelTypes.stringLiteral('font-size: 9px; color: black; background: #fff;'))
  }

  return {
    grouped: {
      title: groupTitle,
      args: [
        ...groupedArgs,
        ...commonArgs,
      ]
    },
    simple: [
      ...simpleArgs,
      ...commonArgs,
    ]
  }
}

function getLevelStyle(level) {
  const commonStyle = 'border-radius: 10px;text-transform: uppercase;'

  switch (level) {
    case 'error':
      return `background: Crimson; color: #fff; ${commonStyle}`
    case 'warn':
      return `background: DarkOrange; color: #fff; ${commonStyle}`
    case 'info':
      return `background: LIGHTSKYBLUE; color: DARKBLUE; ${commonStyle};`
    case 'debug':
      return `background: YELLOW; color: DARKRED; ${commonStyle};`
    case 'log':
    default:
      return `background:GAINSBORO; color: #000; ${commonStyle}`
  }
}

function getMessageStyle(level) {
  return 'color: black;font-size: 16px;'
}

function getLevelmoji({ level, withIcons }) {
  if (!withIcons) {
    return ''
  }

  switch (level) {
    case 'debug':
      return '🐛'
    case 'log':
      return '📝'
    case 'info':
      return 'ℹ️'
    case 'warn':
      return '⚠️'
    case 'error':
      return '❌'
  }
}

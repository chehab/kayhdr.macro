const babelTypes = require('@babel/types')

const LogBuilder  = require('./logBuilder')

/**
 *
 * @param references
 * @param state
 */
module.exports = function kayhdr({ references, state }) {
  const {
    default: defaultImport,
    ...namedImports
  } = references;

  LogBuilder.handleDefault(defaultImport)
  // LogBuilder.for(references)
  // LogBuilder.handleNamedImports(namedImports)
}

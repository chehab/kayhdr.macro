# kayhdr.macro  [Development In Progress]

A [babel-plugin-macros] (https://npm.im/babel-plugin-macros) frontend logger that can have multiple
Transports, Formats, ...etc and with Error Tracking Integration in Production.


## Motivation

Enhance logging for frontend development with a stylized logging to 
present more useful and relative information.
 
In production no console log in favor of adding error tracking system like [Sentry, Bugsnag]. 

## To Do (ToC) 

- [x] [Stylized Logging](#stylized-logging)
  - [x] [Logging levels (debug, log, info, warn, error, danger)](#logging-levels)
  - [x] [Breakpoint (debug) NODE_ENV=development](#breakpoint-on-debug)
  - [x] [Namespace](#namespaces)
  - [x] [Src. Location](#src-location)
  - [x] [Inspect Vars.](#inspect-vars)
  - [x] [Scope Vars.](#scope-vars)
  - [x] [Assert](#assert)
- [ ] [Transports (w/ plugins)](#transports)
- [ ] [Formatters (w/ plugins)](#Formatters)
- [ ] [Integrations Error Tracking (Bugsnag, Sentry)](#integrations-error-tracking)
- [ ] [Open in Editor](#open-in-editor)
- [ ] [Build time Filters](#build-time-filters)
- [ ] [Runtime Filters](#runtime-filters)



## Logging levels

> TBA

```
{
  danger: 0,
  error: 1, 
  warning: 2, 
  notice: 3, 
  info: 4, 
  debug: 5 // (breakpoint if NODE_ENV=development)
}
```

## Stylized logging

Stylized the logging to help unify and present more useful and relative information.

> TBC

### Loglevel

In the Browser Console window, you will get
```markdown
( DEBUG ) [FooBar{}:baz()]                                     [foobar.js:28:12] 
// --^ loglevel
```


### Namespace

part of the unified logging to have the `namespace`,
to represent where is the log statement was used,
 
In the Browser Console window, you will get
```markdown
( DEBUG ) [FooBar{}:baz()]                                     [foobar.js:28:12] 
// -----------^ namespace
```

this log statement was used in class method `baz` in `FooBar` class 

> TBC

### Breakpoint on Debug

if `NODE_ENV=development` all kayhdr debug statements will be a breakpoint  

> TBA (behaviour will change)

### Namespaces

part of the unified logging to have the `namespace`,
to represent where is the log statement was used,
 
In the Browser Console window, you will get
```markdown
─────────── ↓ namepsace
( DEBUG ) [FooBar{}:baz()]                                     [foobar.js:28:12] 
```

this log statement was used in class method `baz` in `FooBar` class 

### Src. Location

logging file name, number and column. not relaying on the source-map. 

In the Browser Console window, you will get
```markdown
────────────────────────────────────────────────────────────────── ↓ loc
( DEBUG ) [FooBar{}:baz()]                                     [foobar.js:28:12] 
...
...
...
<./src/Foo/Foobar/foobar.js:523:45>
────────────────── ↑ full loc
```

this log statement was used `foobar.js` file at line `28` col `12` 


### Inspect Vars.

Add any variable to the `inspect` method and have it part for
the log statement with var name and prettified

In the Browser Console window, you will get
```markdown
( DEBUG ) [FooBar{}:baz()]                                     [foobar.js:28:12] 
Lorem ipsum dolor sit amet, consectetur adipiscing elit
|> Inspect { ... }

<./src/Foo/Foobar/foobar.js:523:45>
```

### Scope Vars.

Call the `scope` method and have all local scope variable be a part of
the log statement with var name and prettified

In the Browser Console window, you will get
```markdown
( DEBUG ) [FooBar{}:baz()]                                     [foobar.js:28:12] 
Lorem ipsum dolor sit amet, consectetur adipiscing elit
|> Scope { ... }

<./src/Foo/Foobar/foobar.js:523:45>
```

### Assert

> TBA

## Transports

The default (current) transport it the browser's console.

> TODO:
> - [x] console transports
> - [ ] plugin system for transports
> - [ ] use multiple transports 
> - [ ] transport for Sentry
> - [ ] transport for Bugsnag

## Formatters

> TODO:
> - [x] console formatter
> - [ ] plugin system for custom formatters
> - [ ] formatters for Sentry
> - [ ] formatters for Bugsnag

## Integrations Error Tracking 

> TODO
> - [ ] Sentry
> - [ ] Bugsnag


# What dose `kayhdr` mean ?

`kayhdr` in Monacan dialect "darija" means "is talking".
